user_pref("browser.uidensity", 1);
user_pref("browser.aboutConfig.showWarning", false);
user_pref("media.ffmpeg.vaapi.enabled", true);
user_pref("apz.gtk.kinetic_scroll.enabled", false);
user_pref("browser.tabs.inTitlebar", 0);
user_pref("places.history.expiration.max_pages", 1000000000);
user_pref("browser.eme.ui.enabled", false);
