-- A tab is displayed as 4 spaces
vim.opt.tabstop = 4
-- Indent with the value of tabstop (which is 4) in normal mode
vim.opt.shiftwidth = 0
-- Indent with the value of shiftwidth (which is the value of tabstop (which is 4)) in insert mode
vim.opt.softtabstop = -1
-- Use spaces instead of tabs
vim.opt.expandtab = true
-- Continue indentation automatically
vim.opt.autoindent = true

-- Display line numbers
vim.opt.number = true
-- Make line numbers relative
vim.opt.relativenumber = true

-- Show whitespace
vim.opt.list = true
-- Use mouse
vim.opt.mouse = 'nv'
-- Set terminal title
vim.opt.title = true
-- Wrap whole words
vim.opt.linebreak = true

-- Use a custom color scheme
vim.cmd('colorscheme codedark')
